``` html
<html>
<body>
    <!-- Two spans, the first will be chosen -->
    <div>
        <span class="sampleClass">Nested span</span>
    </div>
    <span>Simple span</span>

    <!-- Pre element -->
    <div id ="test">
        <pre>Pre</pre>
    </div>
</body>
</html>
```

``` javascript
// result (array): ["body", "div", "span.sampleClass"]
$('span').getDomPath(false)

// result (string): body > div > span.sampleClass
$('span').getDomPath()

// result (array): ["body", "div#test", "pre"]
$('pre').getDomPath(false)

// result (string): body > div#test > pre
$('pre').getDomPath()
```
